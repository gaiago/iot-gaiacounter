# IoT-GaiaCounter
This is the project to run the GaiaCounter on ESP32 device (DOIT ESP32 DEVKIT V1)

## Setup
* Clone this repo
* Download `PlatformIO` for VisualStudioCode
* Install `SLB_USBtoUART` if not installed yet
* Select the board: `DOIT ESP32 DEVKIT V1`
* Run
