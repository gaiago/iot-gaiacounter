#include <WiFi.h>          // Replace with WiFi.h for ESP32
#include <WebServer.h>     // Replace with WebServer.h for ESP32
#include <AutoConnect.h>
#include <LiquidCrystal.h>
#include "certs.h"
#include <WiFiClientSecure.h>
#include <MQTTClient.h>
#include <ArduinoJson.h>


// The name of the device. This MUST match up with the name defined in the AWS console
#define DEVICE_NAME "GaiaCounter"

// The MQTTT endpoint for the device (unique for each AWS account but shared amongst devices within the account)
#define AWS_IOT_ENDPOINT "a8yh6e4868j4s-ats.iot.eu-west-1.amazonaws.com"

// The MQTT topic that this device should publish to
#define AWS_IOT_TOPIC "community/trips"

// How many times we should attempt to connect to AWS
#define AWS_MAX_RECONNECT_TRIES 50


/*The circuit:
 * LCD RS pin to digital pin 23
 * LCD Enable pin to digital pin 22
 * LCD D4 pin to digital pin 21
 * LCD D5 pin to digital pin 19
 * LCD D6 pin to digital pin 18
 * LCD D7 pin to digital pin 5
 * LCD R/W pin to ground
 * LCD VSS pin to ground
 * LCD VCC pin to 5V
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)

 Library originally added 18 Apr 2008
 by David A. Mellis
 library modified 5 Jul 2009
 by Limor Fried (http://www.ladyada.net)
 example added 9 Jul 2009
 by Tom Igoe
 modified 22 Nov 2010
 by Tom Igoe
 modified 7 Nov 2016
 by Arturo Guadalupi

 This example code is in the public domain.

 http://www.arduino.cc/en/Tutorial/LiquidCrystalHelloWorld

*/

WebServer Server;
AutoConnect Portal(Server);
//AutoConnectConfig acConfig;
//acConfig.apid = "GaiaCounter";
//acConfig.psk = "GaiaGo!!";

void rootPage() {
  char content[] = "Hello, world";
  Server.send(200, "text/plain", content);
}

const int rs = 23, en = 22, d4 = 5, d5 = 18, d6 = 19, d7 = 21;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

WiFiClientSecure net = WiFiClientSecure();
MQTTClient client = MQTTClient(512);

void setup() {
  delay(1000);
  Serial.begin(115200);
  Serial.println();

  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("Connecting...");
  
  //Portal.config(acConfig);
  Server.on("/", rootPage);
  if (Portal.begin()) {
    Serial.println("WiFi connected: " + WiFi.localIP().toString());
    lcd.clear();
    lcd.print("WiFi connected: ");
    lcd.setCursor(0, 1);
    lcd.print(WiFi.localIP().toString());

    secondsToWait(5);
    
  }

  connectToAWS();

  client.subscribe(AWS_IOT_TOPIC);
  client.onMessage(messageReceived);

}

void loop() {
    Portal.handleClient();

    client.loop();
    
  lcd.setCursor(0,0);
}

void connectToAWS()
{
    // Configure WiFiClientSecure to use the AWS certificates we generated
    net.setCACert(AWS_CERT_CA);
    net.setCertificate(AWS_CERT_CRT);
    net.setPrivateKey(AWS_CERT_PRIVATE);

    // Connect to the MQTT broker on the AWS endpoint we defined earlier
    client.begin(AWS_IOT_ENDPOINT, 8883, net);

    // Try to connect to AWS and count how many times we retried.
    int retries = 0;
    Serial.print("Connecting to AWS IOT");

    while (!client.connect(DEVICE_NAME) && retries < AWS_MAX_RECONNECT_TRIES) {
        Serial.print(".");
        delay(100);
        retries++;
    }

    // Make sure that we did indeed successfully connect to the MQTT broker
    // If not we just end the function and wait for the next loop.
    if(!client.connected()){
        Serial.println(" Timeout!");
        return;
    }

    // If we land here, we have successfully connected to AWS!
    // And we can subscribe to topics and send messages.
    Serial.println(DEVICE_NAME " connected!");
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("MQTT Connected");
    secondsToWait(5);
}

void sendJsonToAWS()
{
  Serial.println("Sending message to " AWS_IOT_TOPIC );
  StaticJsonDocument<512> jsonDoc;
  JsonObject stateObj = jsonDoc.createNestedObject("state");
  JsonObject reportedObj = stateObj.createNestedObject("reported");

  // Write the temperature & humidity. Here you can use any C++ type (and you can refer to variables)
  reportedObj["temperature"] = 23.76;
  reportedObj["humidity"] = 78.12;
  reportedObj["wifi_strength"] = WiFi.RSSI();

  // Create a nested object "location"
  JsonObject locationObj = reportedObj.createNestedObject("location");
  locationObj["name"] = "Garden";

  char jsonBuffer[512];
  size_t n = serializeJson(jsonDoc, jsonBuffer);

  // Publish the message to AWS
  client.publish(AWS_IOT_TOPIC, jsonBuffer, n);
}

void messageReceived(String &topic, String &payload) {
  Serial.println("INCOMING_MESSAGE: " + topic + " - " + payload);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(payload.substring(0,15));
  lcd.setCursor(0, 1);
  lcd.print(payload.substring(16,30));

//  secondsToWait(10);
//  lcd.clear();
}

void secondsToWait(int seconds){
    unsigned long lastMillis = millis();
    while (millis() - lastMillis < seconds * 1000);
}
